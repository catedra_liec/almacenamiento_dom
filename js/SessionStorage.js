function guardarDatos() {
    sessionStorage.nombre = document.getElementById("nombre").value;
    sessionStorage.apellido = document.getElementById("apellido").value;
}

function recuperarDatos() {
    if ((sessionStorage.nombre != undefined) && (sessionStorage.apellido != undefined)) {
        document.getElementById("datos").innerHTML = "Nombre: " + sessionStorage.nombre + " apellido: " + sessionStorage.apellido;
    } else {
        document.getElementById("datos").innerHTML = "No has introducido tu nombre ni tu apellido";
    }
}