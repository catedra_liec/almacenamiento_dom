window.onload = inicio;

function inicio() {
    inicioBD();
    //recuperar valores
    leerBD();

    var boton = document.getElementById("btnagregar");
    boton.addEventListener("click", function () {
        manejadorClicBoton()
    }, false);

    var borrar = document.getElementById("borrar");
    borrar.addEventListener("click", borrado, false);

    var texto = document.getElementById("txtProducto");
    texto.value = "";
    texto.addEventListener("keydown", function (event) {
        console.log("tecla pulsada");
        if (event.keyCode === 13) {
            event.preventDefault();
            manejadorClicBoton();
        }
    }, false);
}

function manejadorClicBoton() {
    var nuevoli = document.createElement("li");
    var texto = document.getElementById("txtProducto").value;
    if (texto !== "" && texto !== null) {
        nuevoli.textContent = texto;
        addBD(texto);
        document.getElementById("txtProducto").value = "";
        var ul = document.getElementById("listaProductos");
        ul.appendChild(nuevoli);
        var boton = document.getElementById("aviso");
        if (!boton.getAttribute("hidden")) {
            boton.setAttribute("hidden", "");
        }
    } else if (texto === "") {
        var boton = document.getElementById("aviso");
        boton.removeAttribute("hidden");
    }
}

function borrado() {
    //pedir confirmacion
    if (confirm("Confirmar si desea borrar la lista")) {
        //borrar registros de la base de datos
        var open = indexedDB.open("bd", 1);
        open.onsuccess = function (event) {
            thisDB = event.target.result;
            var transaction = thisDB.transaction(["lista"], "readwrite");
            var store = transaction.objectStore("lista");
            store.openCursor().onsuccess = function (event) {
                var cursor = event.target.result;
                if (cursor) {
                    var request = store.delete(cursor.value.name);
                    request.onerror = function (e) {
                        //console.log("se ha borrado con exito");
                    }
                    request.onsuccess = function (e) {
                        //console.log("no se ha borrado con exito");
                    }
                    cursor.continue();
                }
            };
        }
        //Borrar lista
        borrarUl();
    }
}

function borrarUl() {
    var ul = document.getElementById("listaProductos");
    var listado = ul.querySelectorAll("li");
    for (var i = listado.length - 1; i >= 0; i--) {
        listado[i].parentNode.removeChild(listado[i]); //Borramos el elemento del DOM
        if (typeof document.outerHTML !== "undefined") {
            listado[i].outerHTML = ""; //Borramos la memoria asociada al elemento
        }
    }
}


function inicioBD() {
    window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
    window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
    window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
    if (!window.indexedDB) {
        window.alert("Your browser doesn't support a stable version of IndexedDB.");
    }
    var openRequest = indexedDB.open("bd", 1);
    openRequest.onupgradeneeded = function (e) { // cuando es necesario crear las tablas de la base de datos
        console.log("ver");
        thisDB = e.target.result;
        console.log(thisDB);
        if (!thisDB.objectStoreNames.contains("lista")) {
            var os = thisDB.createObjectStore("lista", {
                keyPath: "name"
            }); // crear tabla
        }
    }
    openRequest.onsuccess = function (e) {
        // console.log("se ha creado con exito");
    }
    openRequest.onerror = function (e) {
        // console.log("ha ocurrido algún error");
    }
}

function addBD(nombre) {
    var open = indexedDB.open("bd", 1);
    open.onsuccess = function (event) {
        thisDB = event.target.result;
        var transaction = thisDB.transaction(["lista"], "readwrite");
        var store = transaction.objectStore("lista");

        var cancion = {
            name: nombre
        };
        //console.log(cancion);

        var request = store.add(cancion);
        request.onerror = function (e) {
            // console.log("se ha guardado con exito");
        }
        request.onsuccess = function (e) {
            //  console.log("no se ha guardado con exito");
        }
    }
}

function leerBD() {
    var open = indexedDB.open("bd", 1);
    open.onsuccess = function (event) {
        thisDB = event.target.result;
        var transaction = thisDB.transaction(["lista"], "readonly");
        var store = transaction.objectStore("lista");
        store.openCursor().onsuccess = function (event) {
            var cursor = event.target.result;
            var ul = document.getElementById("listaProductos");
            if (cursor) {
                var nuevoli = document.createElement("li");
                nuevoli.textContent = cursor.value.name;
                ul.appendChild(nuevoli);
                cursor.continue();
            }
        }
    }
}