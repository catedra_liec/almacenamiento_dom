alert("Podrás visualizar a los usuarios registrados, asi como tambien cambiar el color de fondo ");

function GuardarElementos() {
	/*Captura de datos escrito en los inputs*/
	var nom = document.getElementById("nombretxt").value;
	var apel = document.getElementById("apellidotxt").value;

	/*Guardando los datos en el LocalStorage*/
	localStorage.setItem("Nombre", nom);
	localStorage.setItem("Correo", apel);
	/*Limpiando los campos o inputs*/
	document.getElementById("nombretxt").value = "";
	document.getElementById("apellidotxt").value = "";
}

function CargarElementos() {
	for (x = 0; x <= localStorage.length - 1; x++) {
		clave = localStorage.key(x);
		document.getElementById("resultado").innerHTML += ("El campo " + clave + " contiene el valor " + localStorage.getItem(clave) + "<br />" + "<br />");
	}
}

const container = document.getElementById('container'),
	bgStorage = window.localStorage.getItem('background')

if (bgStorage) {
	document.body.style.backgroundColor = bgStorage
}

container.addEventListener('click', e => {
	let buttonBgColor = window.getComputedStyle(e.target).backgroundColor;
	window.localStorage.setItem('background', buttonBgColor)
	if (e.target.tagName === 'BUTTON') {
		document.body.style.backgroundColor = buttonBgColor
	}
})
